#!/bin/bash

# Stop containers and remove them
docker-compose down

# Kill the tail command
pkill -f 'docker-compose logs -f'
