from typing import Any, Callable, List, Tuple, Dict

import streamlit as st
from sqlalchemy import text
import pandas as pd
import networkx as nx
from bokeh.plotting import figure, from_networkx
from bokeh.models import Range1d, Circle, HoverTool, MultiLine, ColumnDataSource, LabelSet
from bokeh.palettes import Spectral4


from scripts import (
    mysql,
    mongodb,
    neo4j,
    s3,
    complex_queries
)

# from .mysql_utils import create_input_form, insert_data_to_mysql

def insert_data_to_mysql(table_name, data):
    engine = mysql.get_engine_with_database(mysql.DATABASE_URL, mysql.db_name)
    with engine.connect() as conn:
        query = text(f"INSERT INTO {table_name} ({', '.join(data.keys())}) VALUES ({', '.join([':' + k for k in data.keys()])})")
        conn.execute(query, data)
        conn.commit()

def create_input_form(table_name):
    st.subheader(f"Add data to {table_name}")
    
    engine = mysql.get_engine_with_database(mysql.DATABASE_URL, mysql.db_name)
    with engine.connect() as conn:
        result = conn.execute(text(f"DESCRIBE {table_name}"))
        columns = [row[0] for row in result.fetchall() if row[0] != 'id']

    data = {}
    for column in columns:
        data[column] = st.text_input(f"Enter {column}")

    col1, col2 = st.columns(2)
    
    with col1:
        if st.button("Submit"):
            insert_data_to_mysql(table_name, data)
            st.success("Data added successfully!")

    with col2:
        if st.button("Return to main page"):
            st.session_state['page'] = 'main'
            st.experimental_rerun()
            

def mysql_metadata() -> Dict[str, List[str]]:
    """Fetches and returns table names from the MySQL database."""
    engine = mysql.get_engine_with_database(mysql.DATABASE_URL, mysql.db_name)
    with engine.connect() as conn:
        result = conn.execute(text("SHOW TABLES;"))
        tables = [row[0] for row in result.fetchall()]
    return {"tables": tables}


def mongo_metadata() -> Dict[str, List[str]]:
    """Fetches and returns collection names from the MongoDB database."""
    db = mongodb.get_database(mongodb.db_name)
    collections = db.list_collection_names()
    return {"collections": collections}


def neo4j_metadata() -> Dict[str, List[str]]:
    """Fetches and returns a tuple of lists containing node labels and relationship types from the Neo4j database."""
    username, password = neo4j.load_neo4j_credentials()
    driver = neo4j.get_neo4j_driver(neo4j.DATABASE_URL, username, password)
    metadata = {}
    with driver.session() as session:
        # Get list of node labels
        label_result = session.run("CALL db.labels()")
        labels = [record["label"] for record in label_result]
        metadata["labels"] = labels

        # Get list of relationship types
        rel_types_result = session.run("CALL db.relationshipTypes()")
        rel_types = [record["relationshipType"] for record in rel_types_result]
        metadata["relationshipTypes"] = rel_types
    driver.close()
    return metadata


def s3_metadata() -> Dict[str, List[str]]:
    """Fetches and returns bucket names from the MinIO storage."""
    username, password = s3.load_minio_credentials()
    client = s3.get_minio_client(access_key=username, secret_key=password)
    buckets = client.list_buckets()
    return {"buckets": [bucket.name for bucket in buckets]}


def fetch_table_data(table_name: str) -> pd.DataFrame:
    engine = mysql.get_engine_with_database(mysql.DATABASE_URL, mysql.db_name)
    with engine.connect() as conn:
        query = text(f"SELECT * FROM {table_name};")
        df = pd.read_sql(query, conn)
    return df


def fetch_collection_data(collection_name: str) -> pd.DataFrame:
    """Fetches data from a specified MongoDB collection into a pandas DataFrame."""
    db = mongodb.get_database()
    collection = db[collection_name]
    data = list(collection.find())
    return pd.DataFrame(data)


def fetch_neo4j_labels() -> list:
    """ Fetches a list of labels from the Neo4j database. """
    username, password = neo4j.load_neo4j_credentials()
    driver = neo4j.get_neo4j_driver(neo4j.DATABASE_URL, username, password)
    with driver.session() as session:
        result = session.run("CALL db.labels()")
        labels = [record["label"] for record in result]
    driver.close()
    return labels


def fetch_label_data(label: str) -> pd.DataFrame:
    """ Fetches data for a specified label in the Neo4j database into a pandas DataFrame. """
    username, password = neo4j.load_neo4j_credentials()
    driver = neo4j.get_neo4j_driver(neo4j.DATABASE_URL, username, password)
    query = f"MATCH (n:{label}) RETURN n LIMIT 25"
    with driver.session() as session:
        result = session.run(query)
        data = [record["n"].items() for record in result]
    driver.close()
    return pd.DataFrame(data)


def display_data(state_key: str, button_label: str, output_func: Callable[[], Any], display_title: str) -> None:
    """
    Display a button in the Streamlit interface that when clicked, fetches and displays data.

    Args:
        state_key: A unique key to store data state in session.
        button_label: The text to display on the button.
        output_func: The function to call to fetch or calculate data.
        display_title: The title to display with the data.
    """
    # if st.button(button_label):
    #     st.session_state[state_key] = True
    #     st.session_state[f'{state_key}_data'] = output_func()
    # if st.session_state.get(state_key):
    #     st.write(display_title, st.session_state[f'{state_key}_data'])
    if st.button(button_label):
        if st.session_state.get(state_key):
            # Toggle off
            st.session_state[state_key] = False
        else:
            # Toggle on and fetch data
            st.session_state[state_key] = True
            st.session_state[f'{state_key}_data'] = output_func()

    if st.session_state.get(state_key):
        st.write(display_title, st.session_state[f'{state_key}_data'])


def _max_width_(rem_width: int = 61):
    rem_width_str = f"max-width: {rem_width}rem;"
    st.markdown(f""" 
                <style> 
                .st-emotion-cache-gh2jqd{{{rem_width_str}}}
                </style>    
                """,
                unsafe_allow_html=True,
    )


def list_files_in_bucket(bucket_name: str):
    """List all files in the specified S3 bucket."""
    username, password = s3.load_minio_credentials()
    client = s3.get_minio_client(access_key=username, secret_key=password)
    files = client.list_objects(bucket_name)
    return [file.object_name for file in files]


def read_file_from_bucket(bucket_name: str, file_name: str):
    """Reads and returns the content of the specified file in the specified S3 bucket."""
    username, password = s3.load_minio_credentials()
    client = s3.get_minio_client(access_key=username, secret_key=password)
    response = client.get_object(bucket_name, file_name)
    return response.data.decode('utf-8')


def visualize_neo4j_graph():
    username, password = neo4j.load_neo4j_credentials()
    driver = neo4j.get_neo4j_driver(neo4j.DATABASE_URL, username, password)
    G = nx.Graph()

    with driver.session() as session:
        query = "MATCH (n)-[r]->(m) RETURN n, r, m"
        result = session.run(query)
        for record in result:
            n = record['n']
            m = record['m']
            r = record['r']

            G.add_node(n.id, label=n.get('name', 'No Name'), color=Spectral4[0])
            G.add_node(m.id, label=m.get('name', 'No Name'), color=Spectral4[1])
            G.add_edge(n.id, m.id, type=r.type)

    driver.close()

    plot = figure(title="Neo4j Graph Visualization",
                  x_range=(-1.1, 1.1), y_range=(-1.1, 1.1),
                  tools="")

    graph_renderer = from_networkx(G, nx.spring_layout, scale=1, center=(0, 0))

    graph_renderer.node_renderer.glyph = Circle(size=25, fill_color='color')

    graph_renderer.edge_renderer.glyph = MultiLine(line_color="#CCCCCC", line_alpha=0.8, line_width=2)
    graph_renderer.edge_renderer.data_source.data['type'] = [G.edges[e]['type'] for e in G.edges()]

    graph_layout = graph_renderer.layout_provider.graph_layout
    labels = [G.nodes[node_id]['label'] for node_id in G.nodes()]
    x, y = zip(*graph_layout.values())

    label_source = ColumnDataSource({'x': x, 'y': y, 'labels': labels})
    labels = LabelSet(x='x', y='y', text='labels', source=label_source, x_offset=10, y_offset=7,
                      text_font_size='8pt', render_mode='canvas')
    plot.add_layout(labels)

    edge_hover_tool = HoverTool(tooltips=[("relationship", "@type")], renderers=[graph_renderer.edge_renderer])
    plot.add_tools(edge_hover_tool)

    plot.renderers.append(graph_renderer)

    return plot


def main() -> None:
    """Main function to display the Streamlit app interface."""
    _max_width_(63)

    st.title(':blue[Course work]: Gor Paramazyan, IU6-21M')

    # # MySQL Metadata and Data Display
    # if st.button('Fetch MySQL Metadata'):
    #     st.session_state['mysql_tables'] = mysql_metadata()['tables']
    #     st.session_state['selected_mysql_table'] = None

    # if 'mysql_tables' in st.session_state:
    #     selected_mysql_table = st.selectbox('Select a MySQL table', st.session_state['mysql_tables'])
    #     if st.button('Show MySQL Data'):
    #         df = fetch_table_data(selected_mysql_table)
    #         st.dataframe(df, use_container_width=True)

    if 'page' not in st.session_state:
        st.session_state['page'] = 'main'

    if st.session_state['page'] == 'main':
        col1, col2 = st.columns(2)
        
        with col1:
            if st.button('Fetch MySQL Metadata'):
                st.session_state['mysql_tables'] = mysql_metadata()['tables']
                st.session_state['selected_mysql_table'] = None

        with col2:
            if st.button('Add MySQL Data'):
                st.session_state['page'] = 'add_data'
                st.experimental_rerun()

        if 'mysql_tables' in st.session_state:
            selected_mysql_table = st.selectbox('Select a MySQL table', st.session_state['mysql_tables'])
            if st.button('Show MySQL Data'):
                df = fetch_table_data(selected_mysql_table)
                st.dataframe(df, use_container_width=True)

        # MongoDB Metadata and Data Display
        if st.button('Fetch MongoDB Metadata'):
            st.session_state['mongo_collections'] = mongo_metadata()['collections']
            st.session_state['selected_mongo_collection'] = None

        if 'mongo_collections' in st.session_state:
            selected_mongo_collection = st.selectbox('Select a MongoDB collection', st.session_state['mongo_collections'])
            if st.button('Show MongoDB Data'):
                df = fetch_collection_data(selected_mongo_collection)
                st.dataframe(df, use_container_width=True)

        # Visualize Neo4j Graph
        if st.button('Fetch Neo4j Metadata'):
            st.session_state['neo4j_meta'] = neo4j_metadata()
            st.session_state['neo4j_data'] = None

        if st.button('Visualize Graph'):
            fig = visualize_neo4j_graph()
            st.bokeh_chart(fig, use_container_width=True)

        # S3 Metadata and Data Display
        if st.button('Fetch S3 Metadata'):
            st.session_state['s3_buckets'] = s3_metadata()['buckets']
            st.session_state['selected_s3_bucket'] = None
            st.session_state['selected_s3_file'] = None

        if 's3_buckets' in st.session_state:
            selected_s3_bucket = st.selectbox('Select an S3 Bucket', st.session_state['s3_buckets'])
            if selected_s3_bucket:
                st.session_state['selected_s3_bucket'] = selected_s3_bucket
                st.session_state['s3_files'] = list_files_in_bucket(selected_s3_bucket)
                selected_s3_file = st.selectbox('Select a File in Bucket', st.session_state['s3_files'])

                if st.button('Show File Content'):
                    file_content = read_file_from_bucket(st.session_state['selected_s3_bucket'], selected_s3_file)
                    st.text_area("File Content", file_content, height=250)
                    
        st.header("Test requests")
        
        if st.button("Projects with owners and task count"):
            st.subheader("SQL Query:")
            st.code(complex_queries.get_projects_with_owners_and_task_count.sql, language='sql')
            st.subheader("Result:")
            df = pd.DataFrame(complex_queries.get_projects_with_owners_and_task_count())
            st.dataframe(df, use_container_width=True, height=550)
            
        if st.button("Resources usage and cost"):
            st.subheader("SQL Query:")
            st.code(complex_queries.get_resources_usage_and_cost.sql, language='sql')
            st.subheader("Result:")
            df = pd.DataFrame(complex_queries.get_resources_usage_and_cost())
            st.dataframe(df, use_container_width=True)
            
        if st.button("Users with projects and resource cost"):
            st.subheader("SQL Query:")
            st.code(complex_queries.get_users_projects_and_resource_cost.sql, language='sql')
            st.subheader("Result:")
            df = pd.DataFrame(complex_queries.get_users_projects_and_resource_cost())
            st.dataframe(df, use_container_width=True)

    
    elif st.session_state['page'] == 'add_data':
        if 'mysql_tables' in st.session_state:
            selected_table = st.selectbox('Select a table to add data', st.session_state['mysql_tables'])
            create_input_form(selected_table)
        else:
            st.error("Please fetch MySQL metadata first")
            if st.button("Return to main page"):
                st.session_state['page'] = 'main'
                st.experimental_rerun()

if __name__ == "__main__":
    main()
