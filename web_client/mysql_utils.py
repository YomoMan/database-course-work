import streamlit as st
from sqlalchemy import text

from scripts import mysql


def insert_data_to_mysql(table_name, data):
    engine = mysql.get_engine_with_database(mysql.DATABASE_URL, mysql.db_name)
    with engine.connect() as conn:
        query = text(f"INSERT INTO {table_name} ({', '.join(data.keys())}) VALUES ({', '.join([':' + k for k in data.keys()])})")
        conn.execute(query, data)
        conn.commit()

def create_input_form(table_name):
    st.subheader(f"Add data to {table_name}")
    
    engine = mysql.get_engine_with_database(mysql.DATABASE_URL, mysql.db_name)
    with engine.connect() as conn:
        result = conn.execute(text(f"DESCRIBE {table_name}"))
        columns = [row[0] for row in result.fetchall() if row[0] != 'id']

    data = {}
    for column in columns:
        data[column] = st.text_input(f"Enter {column}")

    if st.button("Submit"):
        insert_data_to_mysql(table_name, data)
        st.success("Data added successfully!")
        if st.button("Return to main page"):
            st.session_state['page'] = 'main'
            st.experimental_rerun()