#!/bin/bash

# Start container in background
docker-compose up -d

# Wait for containers to start
sleep 3

# Tail logs to file in background
docker-compose logs -f > docker-compose.logs &
