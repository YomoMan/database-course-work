from neo4j import GraphDatabase

from utils import DATABASE_URL, get_neo4j_driver, load_neo4j_credentials
from model import create_graph
from data import fill_data


def main():
    # Load Neo4j credentials
    username, password = load_neo4j_credentials()

    # Connect to Neo4j
    driver: GraphDatabase.driver = get_neo4j_driver(
        DATABASE_URL,
        username=username,
        password=password,
    )

    # Create graph if not exist
    create_graph(driver, [], [])

    # Insert data into graph if it is empty
    fill_data(driver)

    print("Graph initialized and data populated.")
    # Close the driver
    driver.close()


if __name__ == "__main__":
    main()
