from neo4j import GraphDatabase


def create_nodes(driver: GraphDatabase.driver, nodes: list) -> None:
    """Create nodes in the graph."""
    with driver.session() as session:
        for node in nodes:
            session.run("CREATE (n:Node {name: $name})", name=node['name'])


def create_relationships(driver: GraphDatabase.driver, relationships: list) -> None:
    """Create relationships in the graph."""
    with driver.session() as session:
        for relationship in relationships:
            session.run(f"""
                MATCH (a:Node),(b:Node)
                WHERE a.name = $name1 AND b.name = $name2
                CREATE (a)-[r:{relationship['type']}]->(b)
                RETURN r
            """, name1=relationship['from'], name2=relationship['to'])


def create_graph(driver: GraphDatabase.driver, nodes: list, relationships: list) -> None:
    """Create the graph structure."""
    create_nodes(driver, nodes)
    create_relationships(driver, relationships)
