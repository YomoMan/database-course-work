from neo4j import GraphDatabase
import random
from faker import Faker

from model import create_nodes, create_relationships
from scripts.mysql.utils import get_list_of_values

fake = Faker()


def generate_users():
    """ Generate user nodes. """
    users = get_list_of_values("users", "name")
    users = [{'name': user_name} for user_name in users]
    return users


def generate_projects():
    """ Generate project nodes. """
    projects = get_list_of_values("projects", "name")
    projects = [{'name': project_name} for project_name in projects]
    return projects


def generate_resources(n=5):
    """ Generate resource nodes. """
    resources = [{'name': fake.word()} for _ in range(n)]
    return resources


def generate_services(n=5):
    """ Generate service nodes. """
    services = [{'name': f"{fake.word()} Service"} for _ in range(n)]
    return services


def generate_relationships(users, projects, resources, services):
    """ Generate relationships for the graph. """
    relationships = []

    # User works on projects
    for user in users:
        project = random.choice(projects)
        relationships.append({'from': user['name'], 'to': project['name'], 'type': 'WORKS_ON'})

    # Projects use resources
    for project in projects:
        resource = random.choice(resources)
        relationships.append({'from': project['name'], 'to': resource['name'], 'type': 'USES'})

    # Projects have services
    for project in projects:
        service = random.choice(services)
        relationships.append({'from': project['name'], 'to': service['name'], 'type': 'HAS'})

    return relationships


def fill_data(driver: GraphDatabase.driver):
    """ Fill the graph with generated data. """
    users = generate_users()
    projects = generate_projects()
    resources = generate_resources()
    services = generate_services()
    relationships = generate_relationships(users, projects, resources, services)

    # Create nodes and relationships
    create_nodes(driver, users + projects + resources + services)
    create_relationships(driver, relationships)
