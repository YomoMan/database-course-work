import os
from typing import Tuple

from neo4j import GraphDatabase
from dotenv import load_dotenv


DATABASE_URL: str = "bolt://localhost:7687"


def load_neo4j_credentials() -> Tuple[str, str]:
    """ Load Neo4j credentials from the .env file. """
    load_dotenv()
    username = os.getenv("NEO4J_USERNAME", None)
    password = os.getenv("NEO4J_PASSWORD", None)
    return username, password


def get_neo4j_driver(uri: str, username: str, password: str) -> GraphDatabase.driver:
    """Connect to Neo4j database and return the driver."""
    driver = GraphDatabase.driver(uri, auth=(username, password))
    return driver


def delete_all_data(driver: GraphDatabase.driver):
    with driver.session() as session:
        session.run("MATCH (n) DETACH DELETE n")

