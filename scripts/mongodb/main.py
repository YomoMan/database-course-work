from utils import get_database, is_collection_empty
from model import create_collections
from data import fill_data


def main():
    db_name: str = 'cloud_platform'

    db = get_database(db_name)

    # Create collections if not exist
    create_collections(db)

    # Insert data into collections if they are empty
    fill_data(db)

    print("Database initialized and data populated.")


if __name__ == "__main__":
    main()
