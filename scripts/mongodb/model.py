from typing import List

from pymongo.database import Database

collection_names = [
    'logs', 
    'user_sessions', 
    'configurations', 
    'projects', 
    's3_buckets'
]


def create_collections(db: Database, collections: List[str] = None):
    """Create necessary collections if they don't exist in the database."""
    if collections is None:
        collections = collection_names
    existing_collections = db.list_collection_names()
    for collection in collections:
        if collection not in existing_collections:
            db.create_collection(collection)
