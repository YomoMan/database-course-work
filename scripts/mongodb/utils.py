from typing import Tuple
import os

from pymongo import MongoClient
from pymongo.database import Database
from pymongo.collection import Collection
from dotenv import load_dotenv

db_name: str = 'cloud_platform'


def load_credentials() -> Tuple[str, str]:
    """Load MongoDB credentials from the .env file."""
    load_dotenv()
    username = os.getenv("MONGO_USERNAME", None)
    password = os.getenv("MONGO_PASSWORD", None)
    return username, password


def get_database(db_name: str = db_name, port: int = 27017) -> Database:
    """Connect to MongoDB and return the database object."""
    username, password = load_credentials()
    if username and password:
        client = MongoClient(f'mongodb://{username}:{password}@localhost:{port}/')
    else:
        client = MongoClient(f'mongodb://localhost:{port}/')
    return client[db_name]


def is_collection_empty(collection: Collection) -> bool:
    """Check if a MongoDB collection is empty."""
    return collection.count_documents({}) == 0
