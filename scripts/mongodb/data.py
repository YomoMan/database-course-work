from pymongo.database import Database
from faker import Faker
import random

# import utils
from scripts.mysql.utils import get_table_size
from utils import is_collection_empty

fake = Faker()

# Get sizes of MySQL tables
users_size = get_table_size('users')
projects_size = get_table_size('projects')


def generate_logs(n: int = 10):
    """Generate pseudo-data for the logs collection."""
    return [{
        "user_id": fake.random_int(min=1, max=users_size),
        "action": fake.random_element(elements=("login", "logout", "update", "delete")),
        "status": fake.random_element(elements=("success", "failure")),
        "timestamp": fake.iso8601(),
        "details": {
            "IP": fake.ipv4(),
            "device": fake.user_agent()
        }
    } for _ in range(n)]


def generate_user_sessions(n: int = 15):
    """Generate pseudo-data for the user sessions collection. """
    return [{
        "session_id": fake.uuid4(),
        "user_id": fake.random_int(min=1, max=100),
        "start_time": fake.past_datetime(),
        "end_time": fake.future_datetime(),
        "status": fake.random_element(elements=("active", "inactive")),
        "details": {
            "IP": fake.ipv4(),
            "device": fake.user_agent()
        }
    } for _ in range(n)]


def generate_configurations(n: int = 5):
    """Generate pseudo-data for the configurations collection. """
    return [{
        "user_id": fake.random_int(min=1, max=100),
        "config_type": fake.random_element(elements=("interface", "notifications", "privacy")),
        "settings": fake.sentence()
    } for _ in range(n)]


def generate_projects(n: int = 20):
    """Generate pseudo-data for the projects' collection."""
    return [{
        "project_id": fake.random_int(min=1, max=projects_size),
        "name": fake.company(),
        "description": fake.catch_phrase(),
        "owner_id": fake.random_int(min=1, max=users_size),
        "billing_account": {
            "account_id": fake.random_int(min=1000, max=9999),
            "payment_method": fake.credit_card_provider(),
            "billing_address": fake.address()
        },
        "users": [{
            "user_id": fake.random_int(min=1, max=100),
            "permissions": ["admin", "developer", "viewer"][0:fake.random_int(min=1, max=3)]
        } for _ in range(fake.random_int(min=1, max=3))],
        "resources": {
            "s3_buckets": [{
                "bucket_name": fake.word(),
                "region": fake.random_element(elements=["ru-east-1", "ru-west-1", "ru-west-2", "ru-central-1"])
            } for _ in range(fake.random_int(min=1, max=3))],
            "services": {
                "analytics": {
                    "service_type": "Google Analytics",
                    "configurations": {"tracking_id": "UA-" + str(fake.random_int(min=10000, max=99999)) + "-Y"}
                },
                "backup": {
                    "service_type": "AWS Backup",
                    "configurations": {"frequency": "daily", "retention": "30 days"}
                }
            }
        }
    } for _ in range(n)]


def generate_s3_buckets(n: int = 20):
    """Generate pseudo-data for the s3_buckets collection."""
    return [{
        "bucket_id": fake.uuid4(),
        "name": fake.word(),
        "max_storage_mb": random.choice([1000, 5000, 10000, 50000]),
        "used_storage_mb": random.randint(100, 900),
        "storage_type": random.choice(["Standard", "IA", "One Zone-IA", "Glacier"]),
        "region": random.choice(["ru-east-1", "ru-west-1", "ru-west-2", "ru-central-1"]),
        "creation_date": fake.past_datetime(),
        "public_access": fake.boolean(),
        "encryption": {
            "enabled": fake.boolean(),
            "type": random.choice(["AES256", "aws:kms"])
        },
        "versioning": fake.boolean(),
        "logging": {
            "enabled": fake.boolean(),
            "target_bucket": "log-" + fake.word(),
            "target_prefix": fake.word() + "/"
        },
        "tags": {
            "project": fake.word(),
            "owner": fake.name()
        }
    } for _ in range(n)]


def insert_data(db: Database, collection_name: str, data: list) -> None:
    """Insert generated data into the specified collection."""
    
    if is_collection_empty(db[collection_name]):
        db[collection_name].insert_many(data)


data_generation_functions = {
    'logs': generate_logs,
    'user_sessions': generate_user_sessions,
    'configurations': generate_configurations,
    'projects': generate_projects,
    's3_buckets': generate_s3_buckets
}        


def fill_data(db: Database) -> None:
    """
    Generates sample data and inserts it into the collections.

    Args:
        db: The MongoDB Database instance.
    """
    for collection_name, generate_function in data_generation_functions.items():
        if is_collection_empty(db[collection_name]):
            data = generate_function()
            insert_data(db, collection_name, data)
    
