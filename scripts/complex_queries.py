from sqlalchemy import select, func, desc, distinct
from scripts.mysql import get_engine_with_database, DATABASE_URL, db_name
from scripts.mysql.model import projects, users, tasks, compute_resources, resource_allocations


def get_projects_with_owners_and_task_count():
    engine = get_engine_with_database(DATABASE_URL, db_name)
    query = (
        select(
            projects.c.project_id,
            projects.c.name.label('project_name'),
            users.c.name.label('owner_name'),
            func.count(tasks.c.task_id).label('task_count')
        )
        .select_from(
            projects.join(users, projects.c.owner_id == users.c.id)
            .outerjoin(tasks, projects.c.project_id == tasks.c.project_id)
        )
        .group_by(projects.c.project_id, projects.c.name, users.c.name)
        .order_by(desc('task_count'))
    )
    with engine.connect() as conn:
        result = conn.execute(query)
        return result.fetchall()


def get_resources_usage_and_cost():
    engine = get_engine_with_database(DATABASE_URL, db_name)
    query = (
        select(
            compute_resources.c.resource_id,
            compute_resources.c.type,
            compute_resources.c.specifications,
            func.sum(resource_allocations.c.cost).label('total_cost'),
            func.count(distinct(tasks.c.task_id)).label('task_count')
        )
        .select_from(
            compute_resources
            .join(resource_allocations, compute_resources.c.resource_id == resource_allocations.c.resource_id)
            .join(tasks, resource_allocations.c.task_id == tasks.c.task_id)
        )
        .group_by(compute_resources.c.resource_id, compute_resources.c.type, compute_resources.c.specifications)
        .order_by(desc('total_cost'))
    )
    with engine.connect() as conn:
        result = conn.execute(query)
        return result.fetchall()


def get_users_projects_and_resource_cost():
    engine = get_engine_with_database(DATABASE_URL, db_name)
    query = (
        select(
            users.c.id.label('user_id'),
            users.c.name.label('user_name'),
            func.count(distinct(projects.c.project_id)).label('project_count'),
            func.sum(resource_allocations.c.cost).label('total_resource_cost')
        )
        .select_from(
            users
            .outerjoin(projects, users.c.id == projects.c.owner_id)
            .outerjoin(tasks, projects.c.project_id == tasks.c.project_id)
            .outerjoin(resource_allocations, tasks.c.task_id == resource_allocations.c.task_id)
        )
        .group_by(users.c.id, users.c.name)
        .order_by(desc('total_resource_cost'))
    )
    with engine.connect() as conn:
        result = conn.execute(query)
        return result.fetchall()


get_users_projects_and_resource_cost.sql = """
SELECT 
    u.id AS user_id, 
    u.name AS user_name, 
    COUNT(DISTINCT p.project_id) AS project_count,
    SUM(ra.cost) AS total_resource_cost
FROM 
    users u
LEFT JOIN 
    projects p ON u.id = p.owner_id
LEFT JOIN 
    tasks t ON p.project_id = t.project_id
LEFT JOIN 
    resource_allocations ra ON t.task_id = ra.task_id
GROUP BY 
    u.id, u.name
ORDER BY 
    total_resource_cost DESC;
"""


get_projects_with_owners_and_task_count.sql = """
SELECT 
    p.project_id, 
    p.name AS project_name, 
    u.name AS owner_name, 
    COUNT(t.task_id) AS task_count
FROM 
    projects p
JOIN 
    users u ON p.owner_id = u.id
LEFT JOIN 
    tasks t ON p.project_id = t.project_id
GROUP BY 
    p.project_id, p.name, u.name
ORDER BY 
    task_count DESC;
"""


get_resources_usage_and_cost.sql = """
SELECT 
    cr.resource_id, 
    cr.type, 
    cr.specifications, 
    SUM(ra.cost) AS total_cost,
    COUNT(DISTINCT t.task_id) AS task_count
FROM 
    compute_resources cr
JOIN 
    resource_allocations ra ON cr.resource_id = ra.resource_id
JOIN 
    tasks t ON ra.task_id = t.task_id
GROUP BY 
    cr.resource_id, cr.type, cr.specifications
ORDER BY 
    total_cost DESC;
"""
