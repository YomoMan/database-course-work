from sqlalchemy import Engine, Table
from sqlalchemy.exc import SQLAlchemyError
from faker import Faker
import random
from datetime import datetime

import model
import utils
from logger import logger

fake = Faker()


def generate_users_data(n: int = 15) -> list:
    """Generates data for the users table."""
    return [{
        'name': fake.name(),
        'contact_info': fake.email(),
        'role': random.choice(['Developer', 'Manager', 'Analyst'])
    } for _ in range(n)]


def generate_projects_data(n: int = 15) -> list:
    """Generates data for the projects table."""
    return [{
        'owner_id': random.randint(1, n),  # Assuming owner_id's exist in this range
        'name': fake.company(),
        'description': fake.catch_phrase(),
        'created_at': datetime.now()
    } for _ in range(n)]


def generate_compute_resources_data(n: int = 15) -> list:
    """Generates data for the compute_resources table."""
    return [{
        'type': fake.word(ext_word_list=['CPU', 'GPU', 'TPU']),
        'specifications': fake.sentence(),
        'cost_per_hour': round(random.uniform(0.5, 3.0), 2),
        'availability_status': random.choice(['Available', 'In Use', 'Maintenance'])
    } for _ in range(n)]


def generate_tasks_data(n: int = 15) -> list:
    """Generates data for the tasks table."""
    return [{
        'project_id': random.randint(1, n),  # Assuming project_id's exist in this range
        'description': fake.sentence(),
        'status': random.choice(['Not Started', 'In Progress', 'Completed']),
        'start_time': datetime.now(),
        'end_time': datetime.now() + fake.time_delta(end_datetime=None)
    } for _ in range(n)]


def generate_resource_allocations_data(n: int = 15) -> list:
    """Generates data for the resource_allocations table."""
    return [{
        'task_id': random.randint(1, n),  # Assuming task_id's exist in this range
        'resource_id': random.randint(1, n),  # Assuming resource_id's exist in this range
        'start_time': datetime.now(),
        'end_time': datetime.now() + fake.time_delta(end_datetime=None),
        'cost': round(random.uniform(20, 100), 2)
    } for _ in range(n)]


def insert_data(engine: Engine, table_name: str, data: list) -> None:
    """
    Inserts generated data into the specified table.

    Args:
        engine: The SQLAlchemy engine to use for connection.
        table_name: The name of the table to insert data into.
        data: A list of dictionaries, where each dictionary represents a row to insert.
    """
    with engine.connect() as conn:
        conn.execute(table_name.insert(), data)
        conn.commit()


data_generation_functions = {
    'users': generate_users_data,
    'projects': generate_projects_data,
    'compute_resources': generate_compute_resources_data,
    'tasks': generate_tasks_data,
    'resource_allocations': generate_resource_allocations_data
}


def fill_table(engine: Engine) -> None:
    """
    Generates sample data and inserts it into the tables.
    
    Args:
        engine: The SQLAlchemy engine to use for connection.
    """
    tables = model.metadata.tables

    for table_name, insert_function in data_generation_functions.items():
        if utils.is_table_empty(engine, tables[table_name]):
            try:
                data = insert_function(n=15)
                insert_data(engine, tables[table_name], data)
                logger.info(f"Data inserted successfully for {table_name}")
            except SQLAlchemyError as e:
                logger.error(f"Error inserting data into {table_name}: {e}")
                continue
        else:
            logger.info(f"Skipped data insertion for {table_name}: Table is already not empty.")
