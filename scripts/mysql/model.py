from sqlalchemy import MetaData, Table, Column, Integer, String, DateTime, ForeignKey, Numeric

metadata = MetaData()

users = Table(
    'users', metadata,
    Column('id', Integer, primary_key=True, autoincrement=True),
    Column('name', String(255), nullable=False, index=True),
    Column('contact_info', String(255)),
    Column('role', String(100))
)

projects = Table(
    'projects', metadata,
    Column('project_id', Integer, primary_key=True, autoincrement=True),
    Column('owner_id', Integer, ForeignKey('users.id'), nullable=False),
    Column('name', String(255), nullable=False, index=True),
    Column('description', String(255)),
    Column('created_at', DateTime)
)

compute_resources = Table(
    'compute_resources', metadata,
    Column('resource_id', Integer, primary_key=True, autoincrement=True),
    Column('type', String(255), nullable=False, index=True),
    Column('specifications', String(255)),
    Column('cost_per_hour', Numeric(10, 2)),
    Column('availability_status', String(50))
)

tasks = Table(
    'tasks', metadata,
    Column('task_id', Integer, primary_key=True, autoincrement=True),
    Column('project_id', Integer, ForeignKey('projects.project_id'), nullable=False),
    Column('description', String(255)),
    Column('status', String(50), index=True),
    Column('start_time', DateTime),
    Column('end_time', DateTime)
)

resource_allocations = Table(
    'resource_allocations', metadata,
    Column('allocation_id', Integer, primary_key=True, autoincrement=True),
    Column('task_id', Integer, ForeignKey('tasks.task_id'), nullable=False),
    Column('resource_id', Integer, ForeignKey('compute_resources.resource_id'), nullable=False),
    Column('start_time', DateTime),
    Column('end_time', DateTime),
    Column('cost', Numeric(10, 2))
)