from sqlalchemy import Engine

import model
import utils
import data
from logger import logger

# Load MySQL credentials
username, password = utils.load_credentials()

# Connection parameters
DATABASE_URL: str = f"mysql+pymysql://{username}:{password}@localhost"


def main() -> None:
    """
    Main function to:
    - Establish database connection
    - Create the database and tables
    - Fill tables with generated data
    """
    db_name: str = 'cloud_platform'

    # Create the database if it does not exist
    utils.create_database(DATABASE_URL, db_name)

    # Get the SQLAlchemy engine for the database
    engine: Engine = utils.get_engine_with_database(DATABASE_URL, db_name)

    # Create tables
    utils.create_tables(engine, model.metadata)

    logger.info("Database and tables created successfully.")
    
    data.fill_table(engine)
    logger.info("Sample data generated and inserted successfully.")
    
    
if __name__ == '__main__':
    main()
