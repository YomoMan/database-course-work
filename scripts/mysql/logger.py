import logging
import os

# Define the path for the log file
LOG_FILE = os.path.join(os.getcwd(), 'mysql_database_operations.log')

# Set up basic logging configuration
logging.basicConfig(
    filename=LOG_FILE,
    filemode='a',  # Append to the existing log file
    format='%(asctime)s - %(levelname)s - %(message)s',
    level=logging.INFO
)

# Create a logger for use in other files
logger = logging.getLogger('db_logger')