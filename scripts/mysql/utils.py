from typing import Tuple
import os

from sqlalchemy import create_engine, text, Table, Engine, MetaData
from sqlalchemy.sql import select, func
from dotenv import load_dotenv

DATABASE_URL: str = "mysql+pymysql://root:pass@localhost"
db_name: str = 'cloud_platform'


def load_credentials() -> Tuple[str, str]:
    """Load MySQL credentials from the .env file."""
    load_dotenv()
    username = os.getenv("MYSQL_USERNAME", None)
    password = os.getenv("MYSQL_PASSWORD", None)
    return username, password


def create_database(database_url: str, database_name: str) -> None:
    """
    Creates a database if it does not exist.

    Args:
        database_url: The URL to the database server.
        database_name: The name of the database to create.
    """
    engine: Engine = create_engine(database_url, future=True)
    with engine.connect() as conn:
        conn.execute(text(f"CREATE DATABASE IF NOT EXISTS {database_name}"))
        conn.commit()
        

def get_engine_with_database(database_url: str, database_name: str) -> Engine:
    """
    Returns a SQLAlchemy engine for the specified database.

    Args:
        database_url: The URL to the database server.
        database_name: The name of the database for which to get the engine.

    Returns:
        A SQLAlchemy Engine instance connected to the specified database.
    """
    return create_engine(f"{database_url}/{database_name}", future=True)

    
def create_tables(engine: Engine, metadata: MetaData) -> None:
    """
    Creates tables in the database using the provided engine and metadata.

    Args:
        engine: The SQLAlchemy engine connected to the database where tables will be created.
        metadata: The SQLAlchemy metadata object containing table definitions.
    """
    metadata.create_all(engine)


def is_table_empty(engine: Engine, table: Table) -> bool:
    """
    Checks if the specified table is empty.
    
    Args:
        engine: The SQLAlchemy engine to use for connection.
        table: The SQLAlchemy Table object to check.
    
    Returns:
        True if the table is empty, False otherwise.
    """
    with engine.connect() as conn:
        # Execute a SELECT COUNT(*) statement to find if there are any rows
        result = conn.execute(select(func.count()).select_from(table)).scalar()
        return result == 0


def get_list_of_values(table_name: str, column_name: str) -> list:
    """Returns the number of records in the specified MySQL table."""
    import numpy as np
    # DATABASE_URL: str = "mysql+pymysql://root:pass@localhost"
    # db_name: str = 'cloud_platform'

    engine = get_engine_with_database(DATABASE_URL, db_name)
    with engine.connect() as conn:
        result = conn.execute(text(f"SELECT {column_name} FROM {table_name}"))
        return np.unique(np.array(result.all())).flatten().tolist()


def get_table_size(table_name: str) -> int:
    """Returns the number of records in the specified MySQL table."""
    # DATABASE_URL: str = "mysql+pymysql://root:pass@localhost"
    # db_name: str = 'cloud_platform'

    engine = get_engine_with_database(DATABASE_URL, db_name)
    with engine.connect() as conn:
        result = conn.execute(text(f"SELECT COUNT(*) FROM {table_name}"))
        return result.scalar()

