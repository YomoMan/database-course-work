from typing import List

from minio import Minio


def create_buckets(minio_client: Minio, buckets: List[str] = None):
    """ Create necessary buckets if they don't exist in MinIO. """
    if buckets is None:
        buckets = ["projects", "tasks"]

    for bucket in buckets:
        if not minio_client.bucket_exists(bucket):
            minio_client.make_bucket(bucket)
            print(f"Bucket '{bucket}' created.")
        else:
            print(f"Bucket '{bucket}' already exists.")
