import io
from datetime import datetime

from faker import Faker
from minio import Minio

from scripts.mysql.utils import get_list_of_values

fake = Faker()

users = get_list_of_values("users", "name")
projects = get_list_of_values("projects", "name")


def generate_and_upload_data(minio_client: Minio, bucket_name: str, file_count: int = 10):
    """Generate and upload data files to MinIO."""
    for file_idx in range(file_count):
        data = fake.text(max_nb_chars=200)
        current_date: str = datetime.now().strftime("%Y%m%d")

        if bucket_name == "users":
            prefix = fake.word(ext_word_list=users)
        else:
            prefix = fake.word(ext_word_list=projects)
        file_name = f"{prefix}_document_{file_idx}_{current_date}.txt"

        data_bytes = io.BytesIO(data.encode('utf-8'))
        minio_client.put_object(bucket_name, file_name, data_bytes, length=len(data))
        print(f"File '{file_name}' uploaded to bucket '{bucket_name}'.")
