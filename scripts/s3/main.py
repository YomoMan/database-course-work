from utils import get_minio_client, load_minio_credentials
from model import create_buckets
from data import generate_and_upload_data


def main():
    # Connect to MinIO
    access_key, secret_key = load_minio_credentials()
    print(f"Access key: {access_key}")
    print(f"Secret key: {secret_key}")
    minio_client = get_minio_client(access_key=access_key, secret_key=secret_key)

    # Create buckets
    create_buckets(minio_client, ["projects", "tasks"])

    # Generate and upload data
    generate_and_upload_data(minio_client, "projects", 10)
    generate_and_upload_data(minio_client, "tasks", 10)

    print("Data generation and upload to MinIO completed.")


if __name__ == "__main__":
    main()
