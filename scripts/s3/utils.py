import os

from minio import Minio
from dotenv import load_dotenv


def load_minio_credentials():
    """Load MinIO credentials from .env file."""
    load_dotenv()
    return (
        os.getenv("MINIO_ROOT_USER"),
        os.getenv("MINIO_ROOT_PASSWORD")
    )


def get_minio_client(*, access_key: str, secret_key: str, endpoint="localhost:9000") -> Minio:
    """Connect to MinIO and return the client."""
    client = Minio(
        endpoint,
        access_key=access_key,
        secret_key=secret_key,
        secure=False
    )
    return client
