from setuptools import setup, find_packages

setup(
    name='database_course_scripts',
    version='1.0',
    packages=find_packages(include=['scripts', 'scripts.*'])
)